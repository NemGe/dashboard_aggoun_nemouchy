var express = require('express');
var router = express.Router();
const Artist = require("../models/artists");

//Read all artists
router.get('/', function(req, res, next) {
  Artist.find()
  .then(function(artists){
    res.send(artists);
  })
  .catch(err => {
    res.status(500).send({
      message: err.message || 'Error search artists.'
    });
  });
});

//Read all artists
router.get('/All', function(req, res) {
  Artist.find().exec(function (err, artists) {
    res.json(artists);
  });
});

//Get number of Followers per artists
router.get('/Followers', function(req,res) {
  Artist.aggregate([
    {
      $group: {
        _id:"$name",
        total: {
          $sum: "$followers"
        }
      }
    }
  ]).then (artistsFollowers => {
    res.send(artistsFollowers);
  });
})

//Create an artist
router.put("/", function(req, res) {
  const { name, dateOfBirth, followers, albums } = req.body;

  const artist = new Artist({
    name: name,
    dateOfBirth: new Date(dateOfBirth),
    followers: followers,
    albums: albums || null
  });

  artist.save(function(err, artist) {
    if (err) {
      console.error(err);
      res.status(500).json({ msg: "Erreur !!" })
    } else {
      res.json(artist);
    }
  });
});

//Update artist by ID
router.post("/:id", (req,res) => {

  //const { name } = req.body;

  //Trouver le nom
  /*Artist.find({ Name : name}).exec(function (err, artist) {
    res.json(artist);
  });*/

  Artist.findById(req.params.id).exec(function(err, artist){
    if(err){
      if(err.kind === 'ObjectId'){
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating artist with id ' + req.params.id
      });
    }

    if (req.body.name) artist.name = req.body.name;
    if (req.body.dateOfBirth) artist.dateOfBirth = req.body.dateOfBirth;
    if (req.body.followers) artist.followers = req.body.followers;
    if (req.body.albums) artist.albums = req.body.albums;

    artist.save(function(err, artist){
      if(err){
        return res.status(500).send({
          message: 'Error updating artist with id ' + req.params.id
        });
      }
      res.json(artist);
    })
  });
  /*Artists.findByIdAndUpdate(
    req.params.userId,
    {
      name: req.body.name
    }
  )*/
});

//Delete artist
router.delete("/:id", (req,res) => {
  Artist.findByIdAndRemove(req.params.id)
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Artist deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not delete Artist with id ' + req.params.id
      });
    });
});

module.exports = router;

var express = require('express');
var router = express.Router();
const Album = require("../models/album.model");

//Read all albums
router.get('/', function(req, res, next) {
  Album.find()
  .then(function(albums){
    res.send(albums);
  })
  .catch(err => {
    res.status(500).send({
      message: err.message || 'Error search albums.'
    });
  });
});

//Read all albums
router.get('/All', function(req, res) {
  Album.find().exec(function (err, albums) {
    res.json(albums);
  });
});

//Get number of Albums in different genres
router.get('/Genre', function(req,res) {
  Album.aggregate([
    {
      $group: {
        _id:"$genre",
        total: {
          $sum: 1
        }
      }
    }
  ]).then (genreData => {
    res.send(genreData);
  });
})

//Create an Album
router.put("/", function(req, res) {
  const { title, release, genre, cover_url, tracks } = req.body;

  const album = new Album({
    title: title,
    release: new Date(release),
    genre: genre,
    cover_url: cover_url || null,
    tracks: tracks || null
  });

  album.save(function(err, album) {
    if (err) {
      console.error(err);
      res.status(500).json({ msg: "Erreur !!" })
    } else {
      res.json(album);
    }
  });
});

//Update by ID
router.post("/:id", (req,res) => {

  Album.findById(req.params.id).exec(function(err, album){
    if(err){
      if(err.kind === 'ObjectId'){
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating album with id ' + req.params.id
      });
    }

    if (req.body.title) album.title = req.body.title;
    if (req.body.release) album.release = req.body.release;
    if (req.body.genre) album.genre = req.body.genre;
    if (req.body.cover_url) album.cover_url = req.body.cover_url;
    if (req.body.tracks) album.tracks = req.body.tracks;

    album.save(function(err, album){
      if(err){
        return res.status(500).send({
          message: 'Error updating album with id ' + req.params.id
        });
      }
      res.json(album);
    })
  });
});

//Delete artist
router.delete("/:id", (req,res) => {
  Album.findByIdAndRemove(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Album deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not delete user with id ' + req.params.id
      });
    });
});

module.exports = router;

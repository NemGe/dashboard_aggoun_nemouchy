var express = require('express');
var router = express.Router();
const Track = require("../models/tracks");

//Read all tracks
router.get('/', function(req, res, next) {
  Track.find()
  .then(function(tracks){
    res.send(tracks);
  })
  .catch(err => {
    res.status(500).send({
      message: err.message || 'Error search tracks.'
    });
  });
});

//Read all Tracks
router.get('/All', function(req, res) {
  Track.find().exec(function (err, tracks) {
    res.json(tracks);
  });
});

//Get number of Listenings per Tracks
router.get('/Listenings', function(req,res) {
  Track.aggregate([
    {
      $group: {
        _id:"$title",
        total: {
          $sum: "$listenings"
        }
      }
    }
  ]).then (trackListenings => {
    res.send(trackListenings);
  });
})

//Get Likes per Tracks
router.get('/Likes', function(req,res) {
  Track.aggregate([
    {
      $group: {
        _id:"$title",
        total: {
          $sum: "$likes"
        }
      }
    }
  ]).then (trackListenings => {
    res.send(trackListenings);
  });
})

//Get Duration per Tracks
router.get('/Duration', function(req,res) {
  Track.aggregate([
    {
      $group: {
        _id:"$title",
        total: {
          $sum: "$duration"
        }
      }
    }
  ]).then (trackDuration => {
    res.send(trackDuration);
  });
})


//Create a Track
router.put("/", function(req, res) {
  const { title, duration, listenings, likes, featuring } = req.body;

  const track = new Track({
    title: title,
    duration: duration,
    listenings: listenings,
    likes: likes,
    featuring: featuring || null
  });

  track.save(function(err, track) {
    if (err) {
      console.error(err);
      res.status(500).json({ msg: "Erreur !!" })
    } else {
      res.json(track);
    }
  });
});

//Update track by ID
router.post("/:id", (req,res) => {

  Track.findById(req.params.id).exec(function(err, track){
    if(err){
      if(err.kind === 'ObjectId'){
        return res.status(404).send({
          message: 'Track not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating track with id ' + req.params.id
      });
    }

    if (req.body.title) track.title = req.body.title;
    if (req.body.duration) track.duration = req.body.duration;
    if (req.body.listenings) track.listenings = req.body.listenings;
    if (req.body.likes) track.likes = req.body.likes;
    if (req.body.featuring) track.featuring = req.body.featuring;

    track.save(function(err, track){
      if(err){
        return res.status(500).send({
          message: 'Error updating track with id ' + req.params.id
        });
      }
      res.json(track);
    })
  });
});

//Delete Track
router.delete("/:id", (req,res) => {
  Track.findByIdAndRemove(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Track not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Track deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Track not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not Track user with id ' + req.params.id
      });
    });
});

module.exports = router;

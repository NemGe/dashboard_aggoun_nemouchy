var mongoose = require('mongoose');

const TrackSchema = mongoose.Schema({
  title: String,
  duration: Number,
  listenings: Number,
  likes: Number,
  featuring: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artist'
  }
});

module.exports = mongoose.model("Track", TrackSchema);

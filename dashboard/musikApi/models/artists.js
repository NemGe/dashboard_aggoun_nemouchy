var mongoose = require('mongoose');

const ArtistSchema = mongoose.Schema({
  name: String,
  dateOfBirth: Date,
  followers: {
    type: Number
  },
  albums: [{type: mongoose.Schema.Types.ObjectId, ref: 'Album'}]
});

module.exports = mongoose.model("Artist", ArtistSchema);

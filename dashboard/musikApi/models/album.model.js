var mongoose = require('mongoose');

const AlbumSchema = mongoose.Schema({
  title: String,
  release: Date,
  genre: {
    type: String,
    enum: ['Jazz', 'Gospel', 'Funk', 'Rock', 'Pop']
  },
  cover_url: String,
  tracks: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Track'
  }
});

module.exports = mongoose.model("Albums", AlbumSchema);

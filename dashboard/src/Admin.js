import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import FormAddArtist from './Components/Admin/FormAddArtist';
import ListOfAllArtists from './Components/Admin/ListOfAllArtists';
import FormAddAlbums from './Components/Admin/FormAddAlbums';
import DeleteAlbum from './Components/Admin/DeleteAlbum';

import './Admin.css'

export default class Admin extends React.Component {
  render() {
    return (
      <div className="Home">
        <header className="Home-header">
        <Container>
          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="14" md="20" lg="6"><FormAddArtist/></Col>
            <Col xs="24" md="12" lg="6"><ListOfAllArtists/></Col>
          </Row>
          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="14" md="20" lg="6"><FormAddAlbums/></Col>
            <Col xs="14" md="20" lg="6"><DeleteAlbum/></Col>
          </Row>
        </Container>
        </header>
      </div>
    );
  }
}

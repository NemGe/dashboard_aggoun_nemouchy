import React from 'react';
import { BrowserRouter as Switch, Route} from "react-router-dom";
import Home from './Home';
import Admin from './Admin';

export default() => (
  <Switch>
     <Route path="/" exact component={Home}/>
    <Route path="/Admin" exact component={Admin}/>
  </Switch>
);

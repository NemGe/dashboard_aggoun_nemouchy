import React, { Component } from 'react';
import './Home.css'
import CountLikes from './Components/CountLikes/CountLikes';
import CountFollowers from './Components/CountFollowers/CountFollowers';
import Tracks from './Components/Tracks/Tracks';
import Albums from './Components/Albums/Albums';
import Table from './Components/Tables/Table';
import MixedGraph from './Components/MixedChart/MixedChart'
import BarGraph from './Components/BarChart/BarChart'
import NbOfArtists from './Components/NbOfArtists/nbOfArtists'
import RadarChart from './Components/RadarChart/RadarChart'
import PieResponsiveContainer from './Components/PieResponsiveContainer/PieResponsiveContainer'
import SimpleAreaChart from './Components/SimpleAreaChart/SimpleAreaChart'
import SimpleRadialBarChart from './Components/SimpleRadialBarChart/SimpleRadialBarChart'
import CustomeActiveShapePieChart from './Components/CustomeActiveShapePieChart/CustomeActiveShapePieChart'
import { Container, Row, Col } from 'reactstrap';

export default class Home extends Component {
  render() {
    return (
      <div className="Home">
        <header className="Home-header">
        <Container>
          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="12" md="12" lg="6"><NbOfArtists/></Col>
            <Col xs="12" md="12" lg="6"><Albums/></Col>
          </Row>

          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="12" md="12" lg="6"><SimpleAreaChart/></Col>
            <Col xs="12" md="12" lg="6"><RadarChart/></Col>
          </Row>

          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="12" md="12" lg="6"><CustomeActiveShapePieChart/></Col>
            <Col xs="12" md="12" lg="6"><BarGraph/></Col>
          </Row>

        </Container>
        </header>
        <Container>
          <Row style={{marginTop:"20px", marginBottom: "20px"}}>
            <Col xs="20" md="12" lg="12"><Table/></Col>
          </Row>
       </Container>
      </div>
    );
  }
}

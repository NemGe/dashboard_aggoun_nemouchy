import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';
import Albums from '../Components/Albums/Albums';
import Average from '../Components/Average/Average';
import BarChart from '../Components/BarChart/BarChart';
import CountFollowers from '../Components/CountFollowers/CountFollowers';
import CountLikes from '../Components/CountLikes/CountLikes';
import CustomeActiveShapePieChart from '../Components/CustomeActiveShapePieChart/CustomeActiveShapePieChart';
import LastReleases from '../Components/LastReleases/LastReleases';
import MixedChart from '../Components/MixedChart/MixedChart';
import NbOfArtists from '../Components/NbOfArtists/nbOfArtists';
import PieResponsiveContainer from '../Components/PieResponsiveContainer/PieResponsiveContainer';
import RadarChart from '../Components/RadarChart/RadarChart';
import SimpleAreaChart from '../Components/SimpleAreaChart/SimpleAreaChart';
import SimpleRadialBarChart from '../Components/SimpleRadialBarChart/SimpleRadialBarChart';
import Table from '../Components/Tables/Table';
import Tracks from '../Components/Tracks/Tracks';


storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ));

storiesOf('Albums', module)
    .add('Albums', () => <Albums />);
storiesOf('Average', module)
    .add('Average', () => <Average />);
storiesOf('BarChart', module)
    .add('BarChart', () => <BarChart />);
storiesOf('CountFollowers', module)
    .add('CountFollowers', () => <CountFollowers />);
storiesOf('CountLikes', module)
    .add('CountLikes', () => <CountLikes />);
storiesOf('CustomeActiveShapePieChart', module)
    .add('CustomeActiveShapePieChart', () => <CustomeActiveShapePieChart />);
storiesOf('LastReleases', module)
    .add('LastReleases', () => <LastReleases />);
storiesOf('MixedChart', module)
    .add('MixedChart', () => <MixedChart />);
storiesOf('nbOfArtists', module)
    .add('nbOfArtists', () => <nbOfArtists />);
storiesOf('PieResponsiveContainer', module)
    .add('PieResponsiveContainer', () => <PieResponsiveContainer />);
storiesOf('RadarChart', module)
    .add('RadarChart', () => <RadarChart />);
storiesOf('SimpleAreaChart', module)
    .add('SimpleAreaChart', () => <SimpleAreaChart />);
  storiesOf('SimpleRadialBarChart', module)
      .add('SimpleRadialBarChart', () => <SimpleRadialBarChart />);
storiesOf('Table', module)
    .add('Table', () => <Table />);
storiesOf('Tracks', module)
    .add('Tracks', () => <Tracks />);

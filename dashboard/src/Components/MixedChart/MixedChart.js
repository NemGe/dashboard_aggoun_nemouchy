import React, { PureComponent } from 'react';
import {
  ComposedChart, Line, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,
  Legend,
} from 'recharts';

const data = [
  {
    name: '2017', Streams: 6333338, Likes: 4800000,
  },
  {
    name: '2018', Streams: 7895328, Likes: 3600000,
  },
  {
    name: '2019', Streams: 260356, Likes: 16000,
  },
];

export default class MixedGraph extends PureComponent {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/9xopwa9v/';

  render() {
    return (
      <div style={{color:"blue"}}>
        <div style={{ width: '100%', height: 300, borderColor: '#E3E3E3', margin: '7px' }}>
          <ResponsiveContainer>
            <ComposedChart
              width={600}
              height={300}
              data={data}
              margin={{
                top: 20, right: 20, bottom: 20, left: 60,
              }}
              >
              <CartesianGrid stroke="#f5f5f5" />
              <XAxis dataKey="name" stroke="#f5f5f5"/>
              <YAxis stroke="#f5f5f5"/>
              <Tooltip />
              <Legend />
              <Area type="monotone" dataKey="Likes" fill="#A6F5C6" stroke="#33F481" />
              <Line type="monotone" dataKey="Streams" stroke="#ff7300" />
            </ComposedChart>
          </ResponsiveContainer>
        </div>
      </div>
    );
  }
}

import React from 'react';
import { Card, CardText, CardBody, CardHeader} from 'reactstrap';

export default class LastReleases extends React.Component {
  render() {
    return (
      <div class="CountLikes">
      <Card body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
      <CardHeader>Average</CardHeader>
        <CardBody>
          <CardText>3.23 minutes</CardText>
        </CardBody>
      </Card>
    </div>
    );
  }
}

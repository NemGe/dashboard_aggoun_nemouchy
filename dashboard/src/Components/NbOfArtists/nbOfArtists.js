import React from 'react';
import { Card, CardText, CardBody, CardHeader} from 'reactstrap';
import axios from 'axios';

export default class Likes extends React.Component {
  //Constructeur
  constructor(props){
    super(props);

    this.state = {
      artists: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { artists } = this.state;

    return (
      <div style={{color:"blue"}}>
        <div class="NumberArtistsDb">
        <Card body inverse style={{ backgroundColor: '#F9F8FE', borderColor: '#E3E3E3', margin: '40px' }}>
        <CardHeader style={{ backgroundColor: '#431FF6' }}>Total number of Artists</CardHeader>
          <CardBody>
            <CardText style={{ backgroundColor: '#F9F8FE', color: '#431FF6' }}>{artists.length}</CardText>
          </CardBody>
        </Card>
      </div>
      </div>
    );
  }

  getData(){
    axios.get("http://localhost:8000/artists")
    .then((rep) => this.setState({artists: rep.data}));
  }

}

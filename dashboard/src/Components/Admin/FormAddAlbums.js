import React, { Component } from 'react';
import'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';

export default class FormAddAlbums extends Component{

  constructor (props){
    super(props);

    this.state= {
      fields: {},
      errorsDuringInput: {}
    }
  }

  handleInput(){
    let fields = this.state.fields;
    let errorsDuringInput = {};
    let correct = true;

    //Name
    if(!fields["title"]){
      correct = false;
      errorsDuringInput["title"] = "Cannot be empty";
    }

    if(!fields["genre"]){
      correct = false;
      errorsDuringInput["genre"] = "Cannot be empty";
    }

    if(!fields["cover_url"]){
      correct = false;
      errorsDuringInput["cover_url"] = "Cannot be empty";
    }

    this.setState({errorsDuringInput: errorsDuringInput});
    return correct;
  }

  dataToSubmit(e){
    e.preventDefault();
    if(this.handleInput()){
      alert("Album successfully added");
      this.getData();
    }
    else{
      alert("Form not complete");
    }
  }

  handleDataChanged(field, e){
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({fields});
  }

  componentDidMount(){
    this.getData();
  }


  render(){
    return (
      <Form onSubmit = {this.dataToSubmit.bind(this)}>
      <FormGroup row>
      <Label for="title" sm={12} class = "label">Title of Album : </Label>
      <Col sm={10}>
      <Input type="text" ref="title" id="title" onChange={this.handleDataChanged.bind(this, "title")}
      value={this.state.fields["title"]}/>
      <span className="error">{this.state.errorsDuringInput["title"]}</span>
      </Col>
      </FormGroup>

      <FormGroup row>
      <Label for="genre" sm={12} class = "label">Genre : </Label>
      <Col sm={10}>
      <Input type="text" ref="genre" id="genre" onChange={this.handleDataChanged.bind(this, "genre")}
      value={this.state.fields["genre"]}/>
      <span className="error">{this.state.errorsDuringInput["genre"]}</span>
      </Col>
      </FormGroup>

      <FormGroup row>
      <Label for="cover_url" sm={12} class = "label">Cover_url : </Label>
      <Col sm={10}>
      <Input type="text" ref="cover_url" id="cover_url" onChange={this.handleDataChanged.bind(this, "cover_url")}
      value={this.state.fields["cover_url"]}/>
      <span className="error">{this.state.errorsDuringInput["cover_url"]}</span>
      </Col>
      </FormGroup>

      <FormGroup check row>
      <Col sm={{ size: 10, offset: 2 }}>
      <Button type="submit" className="btn btn-warning"> Add Album </Button>
      </Col>
      </FormGroup>
      </Form>
    );
  }

    getData(){
      axios.put("http://localhost:8000/albums/", {
        title: this.state.fields.title,
        genre: this.state.fields.genre,
        cover_url: this.state.fields.coverUrl
      })
    }

}

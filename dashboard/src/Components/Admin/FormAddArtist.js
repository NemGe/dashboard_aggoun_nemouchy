import React, { Component } from 'react';
import'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';

export default class FormulaireArtist extends Component{

  constructor (props){
    super(props);

    this.state= {
      fields: {},
      errorsDuringInput: {}
    }
  }

  handleInput(){
    let fields = this.state.fields;
    let errorsDuringInput = {};
    let correct = true;

    //Name
    if(!fields["name"]){
      correct = false;
      errorsDuringInput["name"] = "Cannot be empty";
    }

    if(!fields["birthday"]){
      correct = false;
      errorsDuringInput["birthday"] = "Cannot be empty";
    }

    if(!fields["followers"]){
      correct = false;
      errorsDuringInput["followers"] = "Cannot be empty";
    }

    this.setState({errorsDuringInput: errorsDuringInput});
    return correct;
  }

  dataToSubmit(e){
    e.preventDefault();
    if(this.handleInput()){
      alert("Artist successfully added");
      this.getData();
    }
    else{
      alert("Form not complete");
    }
  }

  handleDataChanged(field, e){
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({fields});
  }

  componentDidMount(){
    this.getData();
  }


  render(){
    return (
      <Form onSubmit = {this.dataToSubmit.bind(this)}>
      <FormGroup row>
      <Label for="name" sm={12} class = "label">Name of Artist : </Label>
      <Col sm={10}>
      <Input type="text" ref="name" id="name" onChange={this.handleDataChanged.bind(this, "name")}
      value={this.state.fields["name"]}/>
      <span className="error">{this.state.errorsDuringInput["name"]}</span>
      </Col>
      </FormGroup>

      <FormGroup row>
      <Label for="birthday" sm={12} class = "label">Date of Birth : </Label>
      <Col sm={10}>
      <Input type="date" ref="birthday" id="birthday" onChange={this.handleDataChanged.bind(this, "birthday")}
      value={this.state.fields["birthday"]}/>
      <span className="error">{this.state.errorsDuringInput["birthday"]}</span>
      </Col>
      </FormGroup>

      <FormGroup row>
      <Label for="followers" sm={12} class = "label">Followers : </Label>
      <Col sm={10}>
      <Input type="number" ref="followers" id="followers" onChange={this.handleDataChanged.bind(this, "followers")}
      value={this.state.fields["followers"]}/>
      <span className="error">{this.state.errorsDuringInput["followers"]}</span>
      </Col>
      </FormGroup>

      <FormGroup check row>
      <Col sm={{ size: 10, offset: 2 }}>
      <Button type="submit" className="btn btn-warning"> Add Artist </Button>
      </Col>
      </FormGroup>
      </Form>
    );
  }

    getData(){
      axios.put("http://localhost:8000/artists/", {
        name: this.state.fields.name,
        dateOfBirth: this.state.fields.birthday,
        followers: this.state.fields.followers
      })
    }

}

import React, { Component } from 'react';
import'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';

export default class DeleteAlbum extends Component{

  constructor (props){
    super(props);

    this.state= {
      fields: {},
      errorsDuringInput: {}
    }
  }

  handleInput(){
    let fields = this.state.fields;
    let errorsDuringInput = {};
    let correct = true;

    //Name
    if(!fields["id"]){
      correct = false;
      errorsDuringInput["id"] = "Cannot be empty";
    }

    this.setState({errorsDuringInput: errorsDuringInput});
    return correct;
  }

  dataToSubmit(e){
    e.preventDefault();
    if(this.handleInput()){
      alert("Album successfully deleted");
      this.getData();
    }
    else{
      alert("Form not complete");
    }
  }

  handleDataChanged(field, e){
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({fields});
  }

  componentDidMount(){
    this.getData();
  }


  render(){
    return (
      <Form onSubmit = {this.dataToSubmit.bind(this)}>
      <FormGroup row>
      <Label for="id" sm={12} class = "label">Id of Album : </Label>
      <Col sm={10}>
      <Input type="text" ref="id" id="id" onChange={this.handleDataChanged.bind(this, "id")}
      value={this.state.fields["id"]}/>
      <span className="error">{this.state.errorsDuringInput["id"]}</span>
      </Col>
      </FormGroup>

      <FormGroup check row>
      <Col sm={{ size: 10, offset: 2 }}>
      <Button type="submit" className="btn btn-warning"> Delete Album </Button>
      </Col>
      </FormGroup>
      </Form>
    );
  }

    getData(){
      axios.delete("http://localhost:8000/albums/:id", {
        id: this.state.fields.id,
      })
    }

}

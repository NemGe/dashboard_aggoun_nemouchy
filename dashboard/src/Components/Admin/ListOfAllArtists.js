import React from 'react';
import { Table, Button, Col } from 'reactstrap';
import axios from 'axios';

export default class ListOfAllArtists extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      artists: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  Refresher(e){
    e.preventDefault();
    this.getData();
  }

  render() {

    const { artists } = this.state;
    const ListArtists = [];

    for(let i = 0 ; i< artists.length;i++){
      ListArtists.push(
        <tr key={i}>
          <td> {i} </td>
          <td> {artists[i].name}</td>
          <td> {artists[i].dateOfBirth}</td>
          <td> {artists[i].followers}</td>
        </tr>
      )
    }

    return(
      <div>
        <Col>
        <Button style={{marginTop:"20px", marginBottom: "20px"}} type="submit" onClick={this.Refresher.bind(this)} className="btn btn-warning"> Refresh </Button>
        </Col>
        <Table responsive>
          <thead class="thead-dark">
            <tr>
              <th> # </th>
              <th> Name </th>
              <th> Date of Birth </th>
              <th> Followers</th>
            </tr>
          </thead>
          <tbody>
          {ListArtists}
          </tbody>
        </Table>
      </div>
    );
  }


  getData(){
    axios.get("http://localhost:8000/artists")
    .then((rep) => this.setState({artists: rep.data}));
  }

}

import React, { PureComponent } from 'react';
import {
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,
} from 'recharts';
import axios from 'axios';

export default class Example extends PureComponent {
  constructor(props){
    super(props);

    this.state = {
      tracks: [],
    }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { tracks } = this.state;

    return (
      <ResponsiveContainer>
      <div>
      <p style={{color:"white", padding:"0.5em"}}>Number of listenings per tracks</p>
        <AreaChart
          width={460}
          height={200}
          data={tracks}
          margin={{
                top: 15, right: 30, left: 80, bottom: 5,
          }}
        >
          <CartesianGrid />
          <XAxis dataKey="_id" stroke="#f5f5f5"/>
          <YAxis stroke="#f5f5f5"/>
          <Tooltip />
          <Area type="monotone" dataKey="total" stroke="#8884d8" fill="#FFFFFF" />
        </AreaChart>
      </div>
      </ResponsiveContainer>
    );
  }

  getData(){
    axios.get("http://localhost:8000/tracks/Listenings")
    .then((rep) => this.setState({tracks: rep.data}));
  }


}

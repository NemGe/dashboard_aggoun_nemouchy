import React, { PureComponent } from 'react';
import { RadialBarChart, RadialBar, Legend } from 'recharts';
import axios from 'axios';

const style = {
  top: 0,
  left: 350,
  lineHeight: '24px',
};

export default class Example extends PureComponent {
  //static jsfiddleUrl = 'https://jsfiddle.net/alidingling/9km41z5z/';
  constructor(props){
    super(props);

    this.state = {
      tracks: [],
    }
  }

  componentDidMount(){
    this.sync();
  }

  render() {

    const { tracks } = this.state;

    return (
      <RadialBarChart width={500} height={300} cx={150} cy={150} innerRadius={20} outerRadius={140} barSize={10} data={tracks}>
        <RadialBar name="_id" minAngle={15} label={{ position: 'insideStart', fill: '#fff' }} background clockWise dataKey="total" />
        <Legend iconSize={10} width={120} height={140} layout="vertical" verticalAlign="middle" wrapperStyle={style} />
      </RadialBarChart>
    );
  }

  sync(){
    axios.get("http://localhost:8000/tracks/Duration")
    .then((rep) => this.setState({tracks: rep.data}));
  }

}

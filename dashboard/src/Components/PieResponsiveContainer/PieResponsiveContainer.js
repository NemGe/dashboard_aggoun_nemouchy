import React, { PureComponent } from 'react';
import {
  PieChart, Pie, Legend, Tooltip,
} from 'recharts';
import axios from 'axios';

const data01 = [
  { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
  { name: 'Group E', value: 278 }, { name: 'Group F', value: 189 },
];

const data02 = [
  { name: 'Group A', value: 2400 }, { name: 'Group B', value: 4567 },
  { name: 'Group C', value: 1398 }, { name: 'Group D', value: 9800 },
  { name: 'Group E', value: 3908 }, { name: 'Group F', value: 4800 },
];

export default class Example extends PureComponent {
  //static jsfiddleUrl = '//jsfiddle.net/alidingling/6okmehja/';
  constructor(props){
    super(props);

    this.state = {
      tracks: [],
    }
  }

  componentDidMount(){
    this.sync();
  }


  render() {

    const { tracks } = this.state;


    return (
      <PieChart width={400} height={400}>
        <Pie dataKey="total" isAnimationActive={false} data={tracks} cx={200} cy={200} outerRadius={80} fill="#8884d8" label />
        <Tooltip />
      </PieChart>
    );
  }

  sync(){
    axios.get("http://localhost:8000/tracks/Listenings")
    .then((rep) => this.setState({artists: rep.data}));
  }

}

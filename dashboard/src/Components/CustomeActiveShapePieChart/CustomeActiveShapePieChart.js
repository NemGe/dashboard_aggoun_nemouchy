import React, { PureComponent } from 'react';
import { PieChart, Pie, Sector, ResponsiveContainer } from 'recharts';
import axios from 'axios';

const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent, value,
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 10) * cos;
  const my = cy + (outerRadius + 10) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 12;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
      <g>
        <text x={cx} y={cy} dy={1} textAnchor="middle" fill={fill}>{payload._id}</text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 1} y={ey} dy={1} textAnchor={textAnchor} fill="#999">
          {`(Rate ${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
  );
};


export default class Example extends PureComponent {
  //static jsfiddleUrl = 'https://jsfiddle.net/alidingling/hqnrgxpj/';
  constructor(props){
    super(props);

    this.state = {
      tracks: [],
    }
  }

  componentDidMount(){
    this.getData();
  }

  state = {
    activeIndex: 0,
  };

  onPieEnter = (data, index) => {
    this.setState({
      activeIndex: index,
    });
  };

  render() {

    const { tracks } = this.state;

    return (
      <ResponsiveContainer>
      <div style={{color:"blue"}}>
      <p style={{color:"white", padding:"1em"}}>Representation
      of Number of Likes giving total (%)</p>
          <PieChart width={800} height={320}>
            <Pie
              activeIndex={this.state.activeIndex}
              activeShape={renderActiveShape}
              data={tracks}
              cx={250}
              cy={120}
              innerRadius={80}
              outerRadius={100}
              fill="#DBD7FB"
              dataKey="total"
              onMouseEnter={this.onPieEnter}
            />
          </PieChart>
          </div>
        </ResponsiveContainer>
    );
  }

  getData(){
    axios.get("http://localhost:8000/tracks/Likes")
    .then((rep) => this.setState({tracks: rep.data}));
  }

}

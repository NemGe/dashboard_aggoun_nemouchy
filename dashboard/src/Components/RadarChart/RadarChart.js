import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import {
  Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer

} from 'recharts';
import axios from 'axios';

export default class RadarCharty extends Component {
  constructor(props){
    super(props);

    this.state = {
      artists: [],
    }
  }

  componentDidMount(){
    this.sync();
  }

  render() {

    const { artists } = this.state;

    return (
      <ResponsiveContainer>
      <div style={{color:"blue"}}>
      <p style={{color:"white", padding:"0.5em"}}>Number of followers per Artists</p>
        <RadarChart stroke="#f5f5f5" cx={280} cy={120} outerRadius={90} width={500} height={240} data={artists}>
          <PolarGrid stroke="#f5f5f5"/>
          <PolarAngleAxis dataKey="_id" stroke="#f5f5f5"/>
          <PolarRadiusAxis stroke="#f5f5f5"/>
          <Radar name="Name" dataKey="total" fill="#4B45EF" fillOpacity={0.5} stroke="#f5f5f5"/>
        </RadarChart>
      </div>
      </ResponsiveContainer>

    );
  }

  sync(){
    axios.get("http://localhost:8000/artists/Followers")
    .then((rep) => this.setState({artists: rep.data}));
  }
}

import React from 'react';
import { Card, CardText, CardBody, CardHeader} from 'reactstrap';
import axios from 'axios';

export default class Tracks extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      tracks: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { tracks } = this.state;

    return (
      <div class="NumberOfTracks">
      <Card body inverse style={{ backgroundColor: '#F9F8FE', borderColor: '#E3E3E3', margin: '40px' }}>
      <CardHeader style={{ backgroundColor: '#431FF6' }}>Tracks</CardHeader>
        <CardBody>
          <CardText style={{ backgroundColor: '#F9F8FE', color: '#431FF6' }}>{tracks.length}</CardText>
        </CardBody>
      </Card>
    </div>
    );
  }

  getData(){
    axios.get("http://localhost:8000/tracks")
    .then((rep) => this.setState({tracks: rep.data}));
  }

}

import React from 'react';
import { Card, CardBody, CardHeader} from 'reactstrap';
import { Table } from 'reactstrap';

export default class LastReleases extends React.Component {
    render() {
      return (
        <div class="CountLikes">
        <Card body outline color="info" body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
        <CardHeader>Last Releases</CardHeader>
          <CardBody>
          <Table size="sm" hover borderless>
        <thead>
          <tr>
            <th>Album</th>
            <th>Track</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Destiny: The Collection</td>
            <td>Cold in the water</td>
          </tr>
        </tbody>
      </Table>
          </CardBody>
        </Card>
      </div> 
      );
    }
  }
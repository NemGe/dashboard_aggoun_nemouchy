import React from 'react';
import { Card, CardText, CardBody, CardTitle, CardHeader} from 'reactstrap';
import axios from 'axios';


export default class Followers extends React.Component {

  //Constructeur
  constructor(props){
    super(props);

    this.state = {
      artists: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    //const { artists } = this.state;

      return (
        <div class="CountLikes">
          <Card body inverse style={{ backgroundColor: '#F9F8FE', borderColor: '#E3E3E3', margin: '40px' }}>
          <CardHeader style={{ backgroundColor: '#4FE86E' }}>Followers</CardHeader>
            <CardBody>
              <CardText style={{ backgroundColor: '#F9F8FE', color: '#4FE86E' }}> {this.state.artists.length} </CardText>
            </CardBody>
           </Card>
        </div>
    );
    }

    getData(){
      axios.get("http://localhost:8000/artists/All")
        .then((res) => this.setState({ artists: res.data }));
    }
}

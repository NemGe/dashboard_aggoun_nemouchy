import React from 'react';
import { Table } from 'reactstrap';
import axios from 'axios';

export default class Tables extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      tracks: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { tracks } = this.state;
    const listOfAllTracks = [];

    for(let i = 0 ; i< tracks.length;i++){
      listOfAllTracks.push(
        <tr key={i}>
          <td> {i} </td>
          <td> {tracks[i].title}</td>
          <td> {tracks[i].listenings}</td>
          <td> {tracks[i].duration}</td>
          <td> {tracks[i].likes}</td>
        </tr>
      )
    }

    return(
      <div>
        <Table responsive>
          <thead class="thead-dark">
            <tr>
              <th> # </th>
              <th> Title </th>
              <th> listenings </th>
              <th> Duration </th>
              <th> Likes </th>
            </tr>
          </thead>
          <tbody>
          {listOfAllTracks}
          </tbody>
        </Table>
      </div>
    );
  }


  getData(){
    axios.get("http://localhost:8000/tracks")
    .then((rep) => this.setState({tracks: rep.data}));
  }

}

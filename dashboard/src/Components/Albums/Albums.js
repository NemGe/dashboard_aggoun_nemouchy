import React from 'react';
import { Card, CardText, CardBody, CardHeader} from 'reactstrap';
import axios from 'axios';


export default class Albums extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      albums: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { albums } = this.state;

    return (
      <div class="CountLikes">
      <Card body inverse style={{ backgroundColor: '#F9F8FE', borderColor: '#E3E3E3', margin: '40px' }}>
      <CardHeader style={{ backgroundColor: '#4FE86E' }}>Total number of Albums</CardHeader>
        <CardBody>
          <CardText style={{ backgroundColor: '#F9F8FE', color: '#4FE86E' }}>{albums.length}</CardText>
        </CardBody>
      </Card>
    </div>
    );
  }

  getData(){
    axios.get("http://localhost:8000/albums")
    .then((rep) => this.setState({albums: rep.data}));
  }

}

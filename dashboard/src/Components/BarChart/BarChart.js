import React, { PureComponent } from 'react';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,
} from 'recharts';

import axios from 'axios'

export default class BarGraph extends PureComponent {

  constructor(props){
    super(props);

    this.state = {
      albums: []
     }
  }

  componentDidMount(){
    this.getData();
  }

  render() {

    const { albums } = this.state;

    return (
      <ResponsiveContainer>
      <div>
        <div style={{ width: '100%', color:"white", borderColor: '#E3E3E3', margin: '7px'}}>
        <p style={{color:"white", padding:"0.5em"}}>Number of albums per genre</p>
            <BarChart
              color="white"
              width={400}
              height={300}
              data={albums}
              margin={{
              top: 5, right: 30, left: 100, bottom: 5,
              }}
            >
              <CartesianGrid stroke="#f5f5f5" />
              <XAxis dataKey="_id" stroke="#f5f5f5"/>
              <YAxis stroke="#f5f5f5"/>
              <Tooltip />
              <Legend />
              <Bar dataKey="total" fill="#CDC7FB" />
            </BarChart>
        </div>
      </div>
      </ResponsiveContainer>
    );
  }

  getData(){
    axios.get("http://localhost:8000/albums/Genre")
    .then((rep) => this.setState({albums: rep.data}));
  }

}

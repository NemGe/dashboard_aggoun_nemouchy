import React from 'react';
import { Card, CardText, CardBody, CardHeader} from 'reactstrap';
import './CountLikes.css'

export default class Likes extends React.Component {
  render() {
    return (
      <div class="CountLikes">
      <Card body inverse style={{ backgroundColor: '#F9F8FE', borderColor: '#E3E3E3', margin: '40px' }}>
      <CardHeader style={{ backgroundColor: '#431FF6' }}>Likes</CardHeader>
        <CardBody>
          <CardText style={{ backgroundColor: '#F9F8FE', color: '#431FF6' }}>2 000 000</CardText>
        </CardBody>
      </Card>
    </div>
    );
  }
}

import React from "react";
import { BrowserRouter as Router, Route} from "react-router-dom";
import Home from './Home'
import Admin from './Admin'
import NavBar from './Components/NavBar'

function App() {
  return (
    <Router>
      <div>
        <NavBar/>
        <Route exact path="/" component={Home} />
        <Route path="/Admin" component={Admin} />
      </div>
    </Router>
  );
}

function Welcome() {
  return (
    <div>
     <Home/>
    </div>
  );
}

function Database() {
  return (
    <div>
     <Admin/>
    </div>
  );
}

export default App;
